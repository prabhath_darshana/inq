package com.exaltra.core.persistance.mysql;

import com.exaltra.core.util.PropertyLoader;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * Created by prabhathd on 7/19/16.
 */

@Configuration
public class MySqlBeanConfiguration {
    PropertyLoader prop = PropertyLoader.getInstance();

    @Bean
    public DataSource dataSource(){
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(prop.getProperty("db.mysql.driver"));
        ds.setUrl(prop.getProperty("db.mysql.url"));
        ds.setUsername(prop.getProperty("db.mysql.username"));
        ds.setPassword(prop.getProperty("db.mysql.password"));
        return ds;
    }
}
