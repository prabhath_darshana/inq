package com.exaltra.core.persistance.mysql;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

/**
 * Created by darshana on 7/15/16.
 */
public class MySqlDaoSupport {

    final static Logger logger = Logger.getLogger(MySqlDaoSupport.class);
    private JdbcTemplate jdbcTemplate = new JdbcTemplate();
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public void setDatasource(DataSource datasource){
        jdbcTemplate.setDataSource(datasource);
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(datasource);
    }

    public JdbcTemplate getJdbcTemplate(){return jdbcTemplate;}
    public NamedParameterJdbcTemplate getNamedJdbcTemplate(){return namedParameterJdbcTemplate;}

}
