package com.exaltra.core.init;

import com.exaltra.core.auth.config.AuthDaoBeanConfigurations;
import com.exaltra.core.auth.config.AuthServiceBeanConfiguration;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by darshana on 7/22/16.
 */
public class ContextBeanFactory {
    private static final Logger logger = Logger.getLogger(ContextBeanFactory.class);

    private static ContextBeanFactory CLASSBEANLOADER = null;
    private static AnnotationConfigApplicationContext CONTEXT = new AnnotationConfigApplicationContext();


    private ContextBeanFactory(){}

    public static ContextBeanFactory getInstance(){
        if(CLASSBEANLOADER == null){
            CLASSBEANLOADER = new ContextBeanFactory();
            registerConfigClasses();
        }
        return CLASSBEANLOADER;
    }

    public static void registerConfigClasses(){
        logger.debug("Registering config classes...");
        CONTEXT.register(AuthDaoBeanConfigurations.class);
        CONTEXT.register(AuthServiceBeanConfiguration.class);
        CONTEXT.refresh();
    }

    public Object getBean(String beanName){
        return CONTEXT.getBean(beanName);
    }
}
