package com.exaltra.core.init;

import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by prabhathd on 7/29/16.
 */
public class SystemInit implements ServletContextListener {
    private static final Logger logger = Logger.getLogger(SystemInit.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.info("Initiation...");
        logger.debug("###################################################################");
        logger.debug("##########################Begining#################################");
        logger.debug("###################################################################");
        ContextBeanFactory.getInstance();
        logger.debug("###################################################################");
        logger.debug("#############################End###################################");
        logger.debug("###################################################################");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        logger.info("Shutting down...");
    }
}
