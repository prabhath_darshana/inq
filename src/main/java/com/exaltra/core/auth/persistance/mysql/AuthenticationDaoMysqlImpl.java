package com.exaltra.core.auth.persistance.mysql;

import com.exaltra.core.auth.models.User;
import com.exaltra.core.auth.persistance.AuthenticationDao;
import com.exaltra.core.persistance.mysql.MySqlDaoSupport;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by darshana on 7/1/16.
 */
public class AuthenticationDaoMysqlImpl extends MySqlDaoSupport implements AuthenticationDao{

    private static final Logger logger = Logger.getLogger(AuthenticationDaoMysqlImpl.class);


    @Override
    public User addUser(User user) throws Exception{
        String sql = "INSERT INTO users (username, password) VALUES (?, ?)";
        logger.debug("INSERT: " +sql);
        getJdbcTemplate().update(sql, user.getUsername(), user.getPassword());
        return user;
    }

    @Override
    public User getUser(String username) throws Exception{
        String sql = "SELECT userid, username, password FROM users WHERE username=?";
        logger.debug("SELECT: "+sql);
        return getJdbcTemplate().queryForObject(sql, new Object[]{username}, userRowMapper);
    }

    @Override
    public User updateUser(User user) throws Exception {
        return null;
    }

    @Override
    public User deleteUser(User user) throws Exception {
        return null;
    }

    RowMapper<User> userRowMapper = new RowMapper<User>() {
        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            user.setUserid(rs.getInt("userid"));
            user.setUsername(rs.getString("username"));
            user.setPassword(rs.getString("password"));
            return user;
        }
    };
}
