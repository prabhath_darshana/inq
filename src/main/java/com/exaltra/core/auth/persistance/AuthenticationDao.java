package com.exaltra.core.auth.persistance;

import com.exaltra.core.auth.models.User;

/**
 * Created by darshana on 7/1/16.
 */
public interface AuthenticationDao {
    public static final String AUTHDAO = "authenticationDao";

    public User getUser(String username) throws Exception;
    public User addUser(User user) throws Exception;
    public User updateUser(User user)throws Exception;
    public User deleteUser(User user)throws Exception;
}
