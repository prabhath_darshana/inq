package com.exaltra.core.auth.service.impl;

import com.exaltra.core.auth.models.User;
import com.exaltra.core.auth.persistance.AuthenticationDao;
import com.exaltra.core.auth.persistance.mysql.AuthenticationDaoMysqlImpl;
import com.exaltra.core.auth.service.AuthenticationService;
import com.exaltra.core.init.ContextBeanFactory;
import org.apache.log4j.Logger;

import java.util.UUID;

/**
 * Created by darshana on 7/1/16.
 */
public class AuthenticationServiceImpl implements AuthenticationService{
    private static final Logger logger = Logger.getLogger(AuthenticationServiceImpl.class);

    ContextBeanFactory contextBeanFactory = null;

    public AuthenticationServiceImpl(){
        this.contextBeanFactory = ContextBeanFactory.getInstance();
    }

    @Override
    public User authenticate(User user) throws Exception {
        logger.info("Authenticating...");
        AuthenticationDao authenticationDao = (AuthenticationDao) contextBeanFactory.getBean(AuthenticationDao.AUTHDAO);
        User userFromDb = authenticationDao.getUser(user.getUsername());

        if(user.getUsername().equals(userFromDb.getUsername()) &&
                user.getPassword().equals(userFromDb.getPassword())){
            user.setUserid(userFromDb.getUserid());
            user.setAuthKey(UUID.randomUUID().toString());
        }

        return user;
    }

    @Override
    public User addUser(User user) throws Exception {
        return null;
    }

    @Override
    public User updateUser(User user) throws Exception {
        return null;
    }

    @Override
    public User deleteUser(User user) throws Exception {
        return null;
    }

    @Override
    public User deactivateUser(User user) throws Exception {
        return null;
    }
}
