package com.exaltra.core.auth.service;

import com.exaltra.core.auth.models.User;

/**
 * Created by darshana on 7/1/16.
 */
public interface AuthenticationService {
    public static final String AUTHSERVICE = "authenticationService";

    public User authenticate(User user)throws Exception;
    public User addUser(User user)throws Exception;
    public User updateUser(User user)throws Exception;
    public User deleteUser(User user)throws Exception;
    public User deactivateUser(User user)throws Exception;
}
