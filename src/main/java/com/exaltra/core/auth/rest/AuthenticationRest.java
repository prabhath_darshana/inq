package com.exaltra.core.auth.rest;

import com.exaltra.core.auth.models.User;
import com.exaltra.core.auth.service.AuthenticationService;
import com.exaltra.core.init.ContextBeanFactory;
import com.google.gson.Gson;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by darshana on 6/23/16.
 */

@Path("auth")
public class AuthenticationRest {
    final static Logger logger = Logger.getLogger(AuthenticationRest.class);

    ContextBeanFactory contextBeanFactory = null;

    public AuthenticationRest(){
        contextBeanFactory = ContextBeanFactory.getInstance();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/health")
    public String getHealth(){
        logger.info("HIT Health Check");
        return "{\"status\":\"healthy\"}";
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/check")
    public String authenticate(String body){
        logger.debug("AuthenticationRest...");
        String response = "{\"status\":\"failed\"}";
        try {
            Gson gson = new Gson();
            User user = gson.fromJson(body, User.class);
            AuthenticationService authenticationService = (AuthenticationService) contextBeanFactory.getBean(AuthenticationService.AUTHSERVICE);
            user = authenticationService.authenticate(user);
            if(user.getAuthKey() != null) {
                response = "{\"status\":\"authenticated\", \"authKey\":\"" + user.getAuthKey() + "\"}";
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return response;
    }

}
