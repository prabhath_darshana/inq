package com.exaltra.core.auth.config;

import com.exaltra.core.auth.service.AuthenticationService;
import com.exaltra.core.auth.service.impl.AuthenticationServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by darshana on 7/22/16.
 */
@Configuration
public class AuthServiceBeanConfiguration {
    private static final Logger logger = Logger.getLogger(AuthServiceBeanConfiguration.class);

    @Bean
    public AuthenticationService authenticationService(){
        logger.debug("Load bean configuration AuthenticationService:com.exaltra.core.auth.service.impl.AuthenticationServiceImpl");
        return new AuthenticationServiceImpl();
    }
}
