package com.exaltra.core.auth.config;

import com.exaltra.core.auth.persistance.AuthenticationDao;
import com.exaltra.core.auth.persistance.mysql.AuthenticationDaoMysqlImpl;
import com.exaltra.core.persistance.mysql.MySqlBeanConfiguration;
import org.springframework.aop.scope.DefaultScopedObject;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import javax.sql.DataSource;

/**
 * Created by darshana on 7/20/16.
 */
@Configuration
public class AuthDaoBeanConfigurations extends MySqlBeanConfiguration{

    @Bean
    @Scope("prototype")
    public AuthenticationDao authenticationDao(DataSource dataSource){
        AuthenticationDaoMysqlImpl authenticationDaoMysql = new AuthenticationDaoMysqlImpl();
        authenticationDaoMysql.setDatasource(dataSource);
        return authenticationDaoMysql;
    }
}
