package com.exaltra.core.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by prabhathd on 7/19/16.
 */
public class PropertyLoader {
    private static Properties properties;
    private static InputStream inputStream;

    private PropertyLoader(){loadProperty();}

    private static class PropertyLoaderHolder{
        private static final PropertyLoader INSTANCE = new PropertyLoader();
    }

    public static PropertyLoader getInstance(){ return PropertyLoaderHolder.INSTANCE; }

    private static void loadProperty(){
        try{
            properties = new Properties();
            inputStream = PropertyLoader.class.getClassLoader().getResourceAsStream("application.properties");
            properties.load(inputStream);
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            if(inputStream != null){
                try{
                    inputStream.close();
                }catch(IOException ioe){
                    ioe.printStackTrace();
                }
            }
        }
    }

    public Properties getProperties(){ return properties;}

    public String getProperty(String key){return properties.getProperty(key);}
}
